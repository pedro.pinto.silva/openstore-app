# German translation for openstore
# Copyright (c) 2017 Rosetta Contributors and Canonical Ltd 2017
# This file is distributed under the same license as the openstore package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: openstore\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-26 05:14+0000\n"
"PO-Revision-Date: 2019-02-21 18:28+0000\n"
"Last-Translator: Florian Leeber <flori@bin.org.in>\n"
"Language-Team: German <https://translate.ubports.com/projects/openstore/"
"openstore-app/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.1.1\n"
"X-Launchpad-Export-Date: 2017-05-26 05:59+0000\n"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:32
msgid "App details"
msgstr "App-Details"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:43
msgid "Open"
msgstr "Öffnen"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:93
msgid ""
"The installed version of this app doesn't come from the OpenStore server. "
"You can install the latest stable update by tapping the button below."
msgstr ""
"Die installierte Version dieser App kommt nicht vom OpenStore Server. Das "
"neuste stabile Update kann über den Button unten installiert werden."

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:118
msgid "Upgrade"
msgstr "Aktualisieren"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:118
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:305
msgid "Install"
msgstr "Installieren"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:127
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:201
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:587
msgid "Remove"
msgstr "Deinstallieren"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:228
msgid "This app has access to restricted system data, see below for details."
msgstr ""
"Diese App hat Zugang zu kritischen Systemdateien, siehe unten für Details."

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:252
msgid "Description"
msgstr "Beschreibung"

#. TRANSLATORS: Title of the changelog section
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:275
msgid "What's New"
msgstr "Was gibt's Neues"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:294
msgid "Packager/Publisher"
msgstr "Herausgeber"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:295
msgid "OpenStore team"
msgstr "OpenStore Team"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:303
msgid "Installed version"
msgstr "Installierte Version"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:312
msgid "Latest available version"
msgstr "Neuste verfügbare Version"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:321
msgid "First released"
msgstr "Erste Veröffentlichung"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:330
msgid "License"
msgstr "Lizenz"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:331
msgid "N/A"
msgstr "k.A."

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:341
#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:104
msgid "Source Code"
msgstr "Quelltext"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:354
msgid "Get support for this app"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:367
msgid "Donate to support this app"
msgstr ""

#. TRANSLATORS: This is the button that shows a list of all the packages from the same author. %1 is the name of the author.
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:383
#, qt-format
msgid "More from %1"
msgstr "Mehr von %1"

#. TRANSLATORS: This is the button that shows a list of all the other packages in the same category. %1 is the name of the category.
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:398
#, qt-format
msgid "Other apps in %1"
msgstr "Andere Apps von %1"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:404
msgid "Package contents"
msgstr "Paketinhalt"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:484
msgid "AppArmor profile"
msgstr "AppArmor-Profil"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:511
msgid "Permissions"
msgstr "Zugriffsrechte"

#. TRANSLATORS: this will show when an app doesn't need any special permissions
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:530
msgid "none required"
msgstr "keine erforderlich"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:546
msgid "Read paths"
msgstr "Lesepfade"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:563
msgid "Write paths"
msgstr "Schreibpfade"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:579
msgid "Remove package"
msgstr "Paket deinstallieren"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:580
#, qt-format
msgid "Do you want to remove %1?"
msgstr "Möchten Sie %1 deinstallieren?"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:596
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:313
#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:82
msgid "Cancel"
msgstr "Abbruch"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:613
#, qt-format
msgid "%1 GB"
msgstr "%1 GB"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:618
#, qt-format
msgid "%1 MB"
msgstr "%1 MB"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:623
#, qt-format
msgid "%1 kB"
msgstr "%1 kB"

#. TRANSLATORS: %1 is the size of a file, expressed in bytes
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:626
#, qt-format
msgid "%1 bytes"
msgstr "%1 bytes"

#: /home/brian/dev/openstore/openstore-app/openstore/CategoriesPage.qml:24
#: /home/brian/dev/openstore/openstore-app/openstore/CategoriesTab.qml:90
#: /home/brian/dev/openstore/openstore-app/openstore/MainPage.qml:32
msgid "Categories"
msgstr "Kategorien"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:29
#: /home/brian/dev/openstore/openstore-app/openstore/MainPage.qml:31
msgid "Discover"
msgstr "Entdecken"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:51
#: /home/brian/dev/openstore/openstore-app/openstore/SearchTab.qml:28
msgid "Search in OpenStore..."
msgstr "Den OpenStore durchsuchen..."

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:125
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:56
msgid "Update available"
msgstr "Aktualisierung verfügbar"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:126
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:56
msgid "✓ Installed"
msgstr "✓ Installiert"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:145
msgid "OpenStore update available"
msgstr "OpenStore Aktualisierung verfügbar"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:146
msgid "Update OpenStore now!"
msgstr "OpenStore jetzt aktualisieren!"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:152
msgid "Details"
msgstr "Details"

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:106
msgid "Nothing here yet"
msgstr "Hier gibt es noch Nichts zu sehen"

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:106
msgid "No results found."
msgstr "Keine Suchergebnisse."

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:107
msgid "No app has been released in this category yet."
msgstr "In dieser Kategorie wurde bisher keine App veröffentlicht."

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:107
msgid "Try with a different search."
msgstr "Versuchen Sie es mit einem neuem Suchbegriff."

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:27
#: /home/brian/dev/openstore/openstore-app/openstore/MainPage.qml:34
msgid "My Apps"
msgstr "Meine Apps"

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:32
#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:26
msgid "Settings"
msgstr "Einstellungen"

#. TRANSLATORS: %1 is the number of items in a given section ("Available updates" or "Installed apps")
#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:69
#, qt-format
msgid "Available updates (%1)"
msgstr "Verfügbare Aktualisierungen (%1)"

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:69
#, qt-format
msgid "Installed apps (%1)"
msgstr "Installierte Apps (%1)"

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:113
msgid "No apps found"
msgstr "Keine Apps gefunden"

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:114
msgid "No app has been installed from OpenStore yet."
msgstr "Bisher wurden keine Apps aus OpenStore installiert."

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:237
msgid "Warning"
msgstr "Hinweis"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:246
msgid ""
"OpenStore allows installing unconfined applications. Please make sure that "
"you know about the implications of that."
msgstr ""
"OpenStore ermöglicht die Installation uneingeschränkter Apps. Seien Sie sich "
"über die Folgen im Klaren."

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:253
msgid ""
"An unconfined application has the ability to break the system, reduce its "
"performance and/or spy on you."
msgstr ""
"Eine App ohne Zugriffsbeschränkung kann das System beschädigen, die "
"Performance beeinträchtigen oder den Nutzer ausspionieren."

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:260
msgid ""
"While we are doing our best to prevent that by reviewing applications, we "
"don't take any responsibility if something bad slips through."
msgstr ""
"Wir geben uns Mühe, die Apps einer manuellen Prüfung zu unterziehen, können "
"jedoch nicht dafür garantieren, dass wir nichts übersehen."

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:267
msgid "Use this at your own risk."
msgstr "Benutzen Sie diese App auf eigene Gefahr."

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:271
msgid "Okay. Got it! I'll be careful."
msgstr "Ok. Ich habe verstanden! Ich werde vorsichtig sein."

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:278
msgid "Get me out of here!"
msgstr "Nichts wie raus hier!"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:291
msgid "Install app?"
msgstr "App installieren?"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:292
#, qt-format
msgid "Do you want to install %1?"
msgstr "Möchten Sie %1 installieren?"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:333
msgid "App installed"
msgstr "App installiert"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:334
msgid "The app has been installed successfully."
msgstr "Die App wurde erfolgreich installiert."

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:337
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:350
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:365
msgid "OK"
msgstr "OK"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:346
msgid "Installation failed"
msgstr "Installation fehlgeschlagen"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:347
msgid ""
"The package could not be installed. Make sure it is a valid click package."
msgstr ""
"Das Paket konnte nicht installiert werden. Stellen Sie sicher, dass es ein "
"gültiges Clickpaket ist."

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:361
#, qt-format
msgid "Installation failed (Error %1)"
msgstr "Installation fehlgeschlagen (Fehler %1)"

#: /home/brian/dev/openstore/openstore-app/openstore/MainPage.qml:33
#: /home/brian/dev/openstore/openstore-app/openstore/SearchTab.qml:15
msgid "Search"
msgstr "Suche"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:29
msgid "Your passphrase is required to access restricted content"
msgstr ""
"Ihr Passwort ist erforderlich, um auf eingeschränkte Inhalte zuzugreifen"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:30
msgid "Your passcode is required to access restricted content"
msgstr "Ihre PIN ist erforderlich um auf eingeschränkte Inhalte zuzugreifen"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:39
msgid "Authentication failed"
msgstr "Authentifizierung fehlgeschlagen"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:47
msgid "Passphrase required"
msgstr "Passwort erforderlich"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:47
msgid "Passcode required"
msgstr "PIN erforderlich"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:59
msgid "passphrase (default is 0000)"
msgstr "Passwort (Standard: 0000)"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:59
msgid "passcode (default is 0000)"
msgstr "PIN (Standard: 0000)"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:71
msgid "Authentication failed. Please retry"
msgstr "Authentifizierung fehlgeschlagen. Bitte erneut versuchen"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:76
msgid "Authenticate"
msgstr "Authentifizieren"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:58
msgid "Parental control"
msgstr "Kindersicherung"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:64
msgid "Hide adult-oriented content"
msgstr "An Erwachsene gerichtete Inhalte verbergen"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:74
msgid ""
"By typing your password you take full responsibility for showing NSFW "
"content."
msgstr ""
"Durch Eingabe des Passworts übernehmen Sie die volle Verantwortung für die "
"Anzeige von Inhalten für Erwachsene."

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:84
msgid "Developers"
msgstr "Entwickler"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:91
msgid "Manage your apps on OpenStore"
msgstr "Ihre Apps im OpenStore verwalten"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:97
msgid "About OpenStore"
msgstr "Über OpenStore"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:113
msgid "Report an issue"
msgstr "Ein Problem melden"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/BottomEdgePageStack.qml:92
msgid "Back"
msgstr "Zurück"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/BottomEdgePageStack.qml:92
msgid "Close"
msgstr "Schliessen"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:37
msgid "Application"
msgstr "App"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:50
msgid "Scope"
msgstr "Scope"

#. TRANSLATORS: This is an Ubuntu platform service for launching other applications (ref. https://developer.ubuntu.com/en/phone/platform/guides/url-dispatcher-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:64
msgid "URL Handler"
msgstr "URL Handler"

#. TRANSLATORS: This is an Ubuntu platform service for content exchange (ref. https://developer.ubuntu.com/en/phone/platform/guides/content-hub-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:78
msgid "Content Hub Handler"
msgstr "Content Hub Handler"

#. TRANSLATORS: This is an Ubuntu platform service for push notifications (ref. https://developer.ubuntu.com/en/phone/platform/guides/push-notifications-client-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:92
msgid "Push Helper"
msgstr "Push Helper"

#. TRANSLATORS: i.e. Online Accounts (ref. https://developer.ubuntu.com/en/phone/platform/guides/online-accounts-developer-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:106
msgid "Accounts provider"
msgstr "Accountprovider"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/TextualButtonStyle.qml:46
#: /home/brian/dev/openstore/openstore-app/openstore/Components/TextualButtonStyle.qml:54
#: /home/brian/dev/openstore/openstore-app/openstore/Components/TextualButtonStyle.qml:60
msgid "Pick"
msgstr "Auswählen"

#~ msgid "Fetching package list..."
#~ msgstr "Paketliste wird geladen..."
